# :rocket: PowerShell

## 1. Windows Poweshellの概要

### ■ PowerShellコマンドレットの一覧表示

- コマンドレットの確認

      PS> Get-Command

- コマンドレットモジュールの確認

      PS> Get-Module

  >※ ***インストール可能なモジュールは「C:\Windows\System32\WindowsPowerShell\v1.0\MOdule」フォルダー配下で確認可能***

- コマンドレットモジュールのインポート

      PS> Import-Module <Module>

### ■ PowerShellスクリプト実行ポリシー

- スクリプト実行ポリシー一覧

  - **Restricted**
    - 構成ファイルの読み込みやスクリプトの実行を行わない

  - **AllSigned**
    - ***すべてのスクリプト構成と構成ファイルが、信頼された発行元によって署名されている場合にのみ実行する***

  - **RemoteSigned**
    - ***ローカルでさ作成したスクリプトは実行できるが、ダウンロードしたすべてのスクリプトおよび、構成ファイルが信頼された発行元によって署名されている場合にのみ実行する***

  - **Unrestricted**
    - すべての構成ファイルを読み込み、すべてのスクリプトを実行する

  - **Bypass**
    - ローカル、ダウンロードを問わずに実行できる

  - **Undefined**
    - 未定義の状態。規定のスクリプト実行ポリシーが有効なスクリプト実行ポリシーになる

### ■ スクリプト実行ポリシーの確認と変更

- 実行ポリシーの確認

      PS> Get-ExecutionPolicy

- 実行ポリシーの設定変更

      PS> Set-ExecutionPolicy [Restricted | AllSigned | RemoteSigned | Unrestricted | Undefined ]

## 2. PowerShellの基本文法

### ■ コマンドレットの履歴

- コマンド履歴表示

      PS> Get-History

- コマンドライン番号を指定して実行

      PS> Invoke-History <num>

### ■ PowerShellのヘルプ機能

- PowerShellヘルプの更新

      PS> Update-Help

- PowerShellのヘルプ参照

      PS> Help <Cmdlet>

- Get-HelpやHelpのスイッチ

  - 「**-Examples**」・・・使用例だけ表示
  - 「**-Details**」・・・詳細情報も表示
  - 「**-Full**」・・・技術情報を含む完全な情報を表示
  - 「**-Onlin**e」・・・オンラインを表示

### ■ オブジェクトの処理

- コマンドレット間でのオブジェクトの引き渡し（パイプ「|」）

      PS> Get-ChildItem C:\ | ForEach-Object {$_.GetType()}

    >※ 「***ForEach-Object」コマンドレットは複数のオブジェクトの入力を受け取り、それを１個ずつ取り出して処理するコマンド***

- 条件に合うオブジェクトの取り出し

      PS> Get-ChildItem C:\ | Where-Object {$_.PSIcontainer}

    >※ 「***Where-Object」コマンドレットは、指定した条件に合うオブジェクトだけを取り出すことができる***

### ■ オブジェクト操作コマンド

- **Select-Object**  
  ・・・***オブジェクトの一部を指定***する事ができる

      // 最初の5行目までを表示
      Get-Service | Select-Object -First 5

      // Name,Statusプロパティのみ表示
      Get-Service | Select-Object Name, Status 

      // Winで始まるサービスの表示
      Get-Service -Name "Win*"

- **Where-Object**  
  ・・・***コマンドレットで取得されたオブジェクトをフィルター***する事ができる

      // CPU使用率が30以上のプロセス
      Get-Process | Where-Object {$_.CPU -gt 30}

      // プロセス名にappが含まれるものから、Statusがstoppedのものだけ表示
      Get-Service | Where-Object { $_.Name -like '*app*' } | Where-Object { $_.Status -eq 'Stopped' }

- **Group-Object**  
  ・・・***指定したプロパティを簡単にグループ化***して、カテゴリに含まれるアイテム数を算出してくれる

      // Windowsのイベントログ（システム）の直近100件を、Sourceのプロパティを元にグループ化
      Get-Eventlog system -newest 100 | Group-Object -Property Source

- **Sort-Object**  
  ・・・***コマンドレットで取得されたオブジェクトのデータを並び替え***る事ができる

      // 稼働中プロセスの中で、Name,CPUプロパティを選択し、CPUを基準に昇順に並べ替えたもののトップ10を表示
      Get-Process | Select-Object Name,CPU | Sort-Object CPU -Descending | Select-Object -First 10

- **ForEach-Object**  
  ・・・***コマンドレットで取得されたオブジェクトのデータを繰り返し処理***する事ができる

### ■ PowerShellの変数

- 変数に格納した文字列をコマンドとして実行する

      PS> $p = "ping 127.0.0.1"
      PS> Invoke-Expression $p

### ■ 配列

- 配列が保持するオブジェクトメンバーの調べ方

      PS> get-member -InputObject $<array>

### ■ 連想配列

- 連想配列が保持するキーの表示

      PS> $<array>.Keys

- 連想配列が保持する値の表示

      PS> $<array>.Values

- 連想配列のクラス型を調べる

      PS> $<array>.GetType()

- 連想配列のメンバーを調べる

      PS> Get-Member -InputObject $<array>

- 連想配列への新規要素追加

      PS> $<array>.Add("keyword",value)

- 連想配列からの削除

      PS> $<array>.Remove("keyword")

- 読み取り専用連想配列の定義

      PS> Set-Variable -Name <process> -Value <val> -Option <option>

## 3. PowerShellのリモート機能

### ■ リモート機能の有効化

- WinRMサービス状況確認

      Get-Service WinRM

- リモート管理の有効化

      Enable-PSRemoting -Force

  > ***※この操作は管理者のコンピューターとリモート管理を行うコンピューターの両方で実行する必要がある！***

## 4. Windowsシステム管理の基礎

### ■ PSドライブの管理

- PowerShellプロバイダーの確認

      Get-PSProvider

- 現在のPSドライブの確認

      Get-PSDrive

  > ※ ***PowerShellではファイルシステム以外でもPSドライブ軽油でレジストリやサービスにアクセスできる！***

- PSドライブの切り替え

      Set-Location <PSProvider>

- PSドライブの確認

      Get-Location

- PSドライブの割り当て

      New-PSDrive [-Name <ドライブ名>] [-PSProvider <PSプロバイダー名>] [-Root <パス>]

  > ※ ***PowerShellで任意に割り当てたドライブは、PowerShellセッションのみで使用できるため、PowerShellコンソールを閉じると、任意に指定したドライブは使用できなくなる！***  
  > ※ ***ただし、「-Root」でリモートコンピューターの共有フォルダを指定した場合、「-Persistent」スイッチを使用すると、PowerShellセッションを閉じても永続的に使用できるようになる！***

- PSドライブの削除

      Remove-PSDrive -Name <ドライブ名>

### ■ システムの停止と再起動

- コンピューターの起動

      Start-Computer -ComputerName <コンピューター名>

- コンピューターの停止

      Stop-Computer -ComputerName <コンピューター名>

- コンピューターの再起動

      Restart-Computer -ComputerName <コンピューター名>

  > ※ ***停止または再起動では複数のコンピューター指定が可能。複数のコンピューターを指定する場合はコンピューター名を「,」で区切る***  
  > ※ ***他のユーザーがログインしている場合で強制的に再起動を実施する場合は「-Force」スイッチを使用する***

- 資格情報の取得と資格情報を使用した再起動の実施

      // 資格情報を変数へ格納
      $cred = Get-Credential

      // 資格情報を使用して再起動実施
      Restart-Computer -ComputerName <ホスト名> -Force -Credential $cred

      // ドメインユーザーの場合の資格情報
      $cred = Get-Credential -Credential <domain\user>

      // 格納された資格情報のユーザー名を使用する場合
      $username = $cred.UserName

      // 格納された資格情報のパスワードを使用する場合
      $userpass = $cred.Password

### ■ 基本的なシステム情報の収集

- コンピューター名

      $env:COMPUTERNAME

- 現在コンピューターにログオンしているユーザー名

      $env:USERNAME

- カレントユーザーがログオンしているドメイン名

      $env:USERDOMAIN

### ■ システム構成と作業環境の調査

- 更新プログラムとサービスパックの確認

      Get-HotFix [-ComputerName <コンピューター名>] [-Credential <資格情報>]      

- ファイルでコンピューター名を指定したホットフィックスの確認

      Get-HotFix -ComputerName (Get-Content <コンピューター名を記述したファイルパス>)

  > ※ ***「コンピューター名を記述したファイルパスは、テキストファイルで１行ずつコンピューター名を記述し、ネットワーク共有フォルダーなどの常にアクセスできる場所に保存しておく***

- 資格情報を指定したホットフィックスの確認

      Get-HotFix -Credential (Get-Credential) -ComputerName <コンピューター名>

- 変数を使用したホットフィックスの確認

      // コンピューター名取得
      $comp = Get-Content <コンピューター名を記述したファイル名>

      // 資格情報
      $cred = Get-Credential

      Get-HotFix -Credential $cred -ComputerName $comp

### ■ システムの詳細情報の取得

- OSの情報の取得

      Get-WmiObject -Class Win32_OperatingSystem [-ComputerName <コンピューター名>] [-Credential <資格情報>]

- コンピューターの情報取得

      Get-WmiObject -Class Win32_ComputerSystem | Format-List *

- 使用可能なユーザーグループの確認

      Get-WmiObject -Class Win32_UserAccount | Format-List *

### ■ システムハードウェアの評価

- ファームウェアのバージョンと状態の確認

      Get-WmiObject -Class Win32_BIOS | Format-List *

### ■ 物理メモリとプロセッサの確認

- 物理メモリ情報の取得

      Get-WmiObject -Class Win32_PhysicalMemory | Format-List *

- プロセッサの情報の取得

      Get-WmiObject -Class Win32_Processor | Format-List *

### ■ デバイスドライバーの確認と管理

- すべてのWindowsドライバーの状態表示

      Get-WindowsDriver -Online -All

- サードパーティドライバーの表示

      Get-WindowsDriver -Online

- ドライバーの詳細表示

      Get-WindowsDriver -Online -Driver <ドライバー名>

### ■ システムサービスの操作

- サービス情報の出力

      // リダイレクトでファイルへ出力
      Get-Service | Where-Object{$_.Status -eq "running"} > C:\logs\RunServices.txt

      // Out-Fileコマンドレットを使用してファイルへ出力する
      Get-Service | Where-Object{$_.Status -eq "Running"} | Out-File -FilePath C:\logs\RunSercices.txt

      // CSVファイルとして出力
      Get-Service | Where-Object{$_.Status -eq "Running"} | Select-Object Status,Name,DisplayName | Export-Csv -Path C:\logs\RunService.csv

- サービスの開始モード

  - **Boot**  
  ・・・OSのローダーによってデバイスドライバーが開始される

  - **System**  
  ・・・OSの初期化プロセスによってデバイスドライバーが開始される

  - **Auto(自動)**  
  ・・・システム起動時にサービスコントロールマネージャー(SCM)によってサービスが自動で起動される

  - **Auto(遅延開始)**  
  ・・・システム起動後に適切な時間をおいてサービスコントロースマネージャー(SCM)によってサービスが自動で起動される

  - **Manual(手動)**  
  ・・・サービスを手動で開始できるようにする

  - **Disable(無効)**  
  ・・・サービスを無効とし、開始できないようにする

## 5. TCP/IPネットワークの構成と管理

### ■ ネットワークアダプタの確認

- ネットワークアダプターのコマンド確認

      Get-Command -Module NetAdapter

- ネットワークアダプターの確認

      Get-NetAdapter

- ネットワークアダプターの操作

      // 有効化
      Enable-NetAdapter

      // 無効化
      Disable-Netadapter

      // 再起動
      Restart-Netadapter

### ■ TCP/IPの構成と確認

- TCP/IPパラメータのコマンド

      // NetTCPIPモジュールで提供されるコマンド
      Get-Command -Module NetTCPIP

      // DnsClientモジュールで提供されるコマンド
      Get-Command -Module DnsClient

- IPアドレス構成

      // IPアドレス新規設定
      New-NetIPAddress [[-InterfaceAlias <アダプター名>] | [-InterfaceIndex <インデックス番号>]] [-AddressFamily <IPバージョン>] [-IPAddress <IPアドレス>] [-PrefixLenght <サブネットマスク>] [-DefaultGateway <デフォルトゲートウェイ>]

      // IPアドレス削除
      Remove-NetIPAddress [[-InterfaceAlias <アダプター名>] | [-InterfaceIndex <インデックス番号>]] [-AddressFamily <IPバージョン>] [-IPAddress <IPアドレス>] [-DefaultGateway <デフォルトゲートウェイ>] [-Confirm:0]

      // IPアドレス取得
      Get-NetIPAddress [[-InterfaceAlias <アダプター名>] | [-InterfaceIndex <インデックス番号>]] [-AddressFamily <IPバージョン>]

### ■ DNSサーバーアドレス構成

- DNSサーバー設定

      // 設定
      Set-DnsClientServerAddress [[-InterfaceAlias <アダプター名>] | [-InterfaceIndex <インデックス番号>]] [[-ServerAddressed <DNSアドレス>] | [-ResetServerAddress]] [-AddressFamily <IPバージョン>] [-Validate]

      // 情報取得
      Get-DnsClientServerAddress [[-InterfaceAlias <アダプター名>] | [-InterfaceIndex <インデックス番号>]] [-AddressFamily <IPバージョン>]

### ■ DHCPクライアント構成

- DHCPクライアント設定

      // 設定
      Set-NetIPInterface [[-InterfaceAlias <アダプター名>] | [-InterfaceIndex <インデックス番号>]] [-AddressFamily <IPバージョン>] [-Dhcp <Enabled / Disabled>]

      // 情報取得
      Get-NetIPInterface [[-InterfaceAlias <アダプター名>] | [-InterfaceIndex <インデックス番号>]] [-AddressFamily <IPバージョン>]

  > ※ ***デフォルトゲートウェイ、DNSサーバの設定は消去されない！個別に消去する必要あり！***

### ■ Windowsファイアウォールの構成

- Windowsファイアウォール概要

  - **ドメインプロファイル**  
  ・・・コンピューターが***Active Directoryドメインサービスドメインに接続しているとき***に適用されるプロファイル。接続場所例：セキュリティが保たれた会社環境

  - **プライベートプロファイル**  
  ・・・コンピューターが***ワークグループまたはホームネットワークに接続しているとき***に適用されるプロファイル。接続場所例：セキュリティがある程度保たれた自宅環境

  - **パブリックプロファイル**  
  ・・・コンピューターがパブリックネットワークに接続している時に適用されるプロファイル。接続場所例：公共の駅、ファストフード店

  > ※***「セキュリティが強化されたWindowsファイアウォール」では、コンピューターがAD DSを検出すると、プライベートプロファイルまたはパブリックプロファイルから、自動的にドメインプロファイルに切り替わる！逆も然り！***

### ■ Windowsファイアウォールのコマンド

- NetSecurityモジュールで提供されるコマンド

      Get-Command -Module NetSecurity

- Windowsファイアウォール動作の構成

      Get-NetFirewallProfile [[-Name] <プロファイル名>]

      Set-NetFirewallProfile [[-Name] <プロファイル名>] [-Enabled <True / False>]

- Windowsファイアウォールの状態一覧確認

      Get-NetFirewallProfile | Format-Table

- プロファイルの有効化 / 無効化

      Set-NetFirewallProfile -Name <Domain or Private or Public> -Enabled <true or false>

- 既存のファイアウォール規則の構成

      // 既存規則の確認
      Get-NetFirewallRule [-DisplayName <規則名>]
                          [-DisplayGroup <規則のグループ名>]
                          [-Action <Allow / Block>]
                          [-Description <説明>]
                          [-Direction <Inbount / Outbound>]
                          [-Enabled <True / False>]

      // 既存規則の変更
      Set-NetFirewallRule [-DisplayName <規則名>]
                          [-DisplayGroup <規則のグループ名>]
                          [-Action <Allow / Block>]
                          [-Description <説明>]
                          [-Direction <Inbount / Outbound>]
                          [-Enabled <True / False>]

- 任意のファイアウォール規則の構成

      New-NetFirewallRule [-DisplayName <規則名>]
                          [-DisplayGroup <規則のグループ名>]
                          [-Action <Allow / Block>]
                          [-Description <説明>]
                          [-Direction <Inbount / Outbound>]
                          [-Enabled <True / False>]
                          [-Profile <プロファイル名>]
                          [-LocalPort <ポート番号>]
                          [-Protocol <プロトコル>]
                          [-Program <プログラム名>]

  > ※ ***プロファイル名には「Domain」「Private」「Public」の他「Any」も設定可能***  
  > ※ ***プロトコルには「TCP」「UDP」「ICMPv4」「ICMPv6」などを指定する***  
  > ※ ***使用するポートが不明なアプリケーションの場合、アプリケーションのプログラム名でファイアウォール規則を作成する***

- 任意のファイアウォール規則の構成確認

      // 許可したポート情報を含む詳細の確認
      Get-NetFirewallRule -DisplayName <規則名> | Get-NetFirewallPortFilter

      // 許可したアプリケーション情報を含む詳細の確認
      Get-NetFirewallRule -DisplayName <規則名> | Get-NetFirewallApplicationFilter

## 6. イベントの記録、追跡、監視

### ■ イベントログの概要

- **Windowsログ**
  - すべてのWindowsに共通のイベントログが含まれる

- **アプリケーションとサービスログ**
  - ユーザーが新規作成したイベントログや、役割や機能の追加により作成されるイベントログが含まれる。既定の状態でも最低限のイベントログが含まれる

### ■ イベント情報の取得

- 基本的なイベント情報の取得

      Get-WinEvent [-LogName <イベントログ名>]
                   -ListLog <イベントログ名>
                   [-ComputerName <コンピューター名>]
                   [-Credential <管理者の資格情報>]
                   [-MaxEvents <表示する最大数>ー
                   [-Oldest]
                   [-FilterXml <完全なXMLフィルター>]
                   [-FilterXPath <管理XMLフィルター>]

  > ※「***-ListLog」は、指定したイベントログ名の一覧を表示！***  
  > ※「***-LogName」は、指定したイベントログのイベントエントリを表示！***

### ■ イベントの記録

- イベントログの作成と削除

      // イベントログの新規作成
      New-EventLog [-LogName <イベントログ名>]
                   -Source <イベントソース名>
                   [-ComputerName <コンピューター名>]

      // イベントログの削除
      Remove-EventLog [-LogName <イベントログ名>]
                      -Source <イベントソース名>
                      [-ComputerName <コンピューター名>]
  
- イベントエントリの書き込み

      Write-EventLog -LogName <イベントログ名>
                    [-EntryType <レベルの種類>]
                    -Source <ソース名>
                    -EventID <イベントID>
                    -Category <タスクのカテゴリ>
                    -Message <メッセージ>
                    [-ComputerName <コンピューター名>]

  > ※ ***「-EntryType」に関しては「Error」「Warning」「Information」「SuccessAudit」「FailureAudit」から指定する。指定がない場合は「Information」になる***

- イベントログのオプション設定

      LimitEventLog [-LogName <イベントログ名>]
                    [-MaximumSize <最大サイズ>]
                    [-OverFlowAction <最大時の動作>]
                    [-RetentionDays <保持日数>]
                    [-ComputerName <コンピューター名>]

  > ※ ***イベントログの最大サイズや、イベントログのサイズが最大に達したときの動作の設定***

- イベントログの削除

      Clear-EventLog [-LogName <イベントログ名>]
                     [-ComputerName <コンピューター名>]

  > ※ ***個別のイベントログエントリ事の削除はできない！イベントログ単位での削除***

## 7. 役割と機能の管理

### ■ 役割と機能の概要

- **役割(Role)**

  - Windows Serverに追加できる主要なサーバーサービスのソフトウェアコンポーネント

- **役割サービス(Role Service)**

  - 役割を構成するための個々のソフトウェアコンポーネント

- **機能(Feature)**

  - Windows Serverに追加できる補助的なサーバーサービスのソフトウェアコンポーネント

## ■ 役割と機能の管理

- 役割と機能の確認

      Get-WindowsFeature [<コンポーネント名>]
                         [-ComputerName <コンピューター名>]
                         [-Credential <管理者名とパスワード>]

- 役割と機能の追加

      Install-WindowsFeature <コンポーネント名>
                             [-ComputerName <コンピューター名>]
                             [-Credential <管理者名とパスワード>]
                             [-IncludeAllSubFeature]
                             [-Source]
                             [-Restart]
                             [-IncludeManagementTools]
                             [-Source]
                             [-Restart]

- 役割と機能の削除

      Uninstall-WindowsFeature <コンポーネント名>
                               [-ComputerName <コンピューター名>]
                               [-Credential <管理者名とパスワード>]
                               [-IncludeManagementTools]
                               [-Remove]
                               [-Restart]

## 8. ディスクとファイルの管理

### ■ ファイルサービスのインストール

- [ファイルサーバー]の役割サービスのインストール

      Install-WindowsFeature FS-FileServer

- [データ重複除去]の役割サービスのインストール

      Install-WindowsFeature FS-Data-Deplication

### ■ ディスク管理

- ディスクやパーティションに関するコマンドレットの確認

      Get-Command -Module storage

- ディスク操作関連コマンドレット一覧

  |             | New-                | Set-                 | Get-                 | Add-                 | Repair-                | Remove-                 |
  | ----------- | ------------------- | -------------------- | -------------------- | -------------------- | ---------------------- | ----------------------- |
  | Volume      |                     | **Set-Volume**       | **Get-Volume**       |                      | **Repair-Volume**      | **Remove-Volume**       |
  | Partition   | **New-Partition**   | **Set-Partition**    | **Get-Partition**    |                      |                        | **Remove-Partition**    |
  | Disk        |                     | **Set-Disk**         | **Get-Disk**         |                      |                        |                         |
  | VDisk       | **New-VirtualDisk** | **Set-VirtualDisk**  | **Get-VirtualDisk**  |                      | **Repair-VirtualDisk** | **Remove-VirtualDisk**  |
  | StorageTier | **New-StorageTier** | **Set-StorageTier**  | **Get-StorageTier**  |                      |                        |                         |
  | StoragePool | **New-StoragePool** | **Set-StoragePool**  | **Get-StoragePool**  |                      |                        |                         |
  | PDisk       |                     | **Set-PhysicalDisk** | **Get-PhysicalDisk** | **Add-PhysicalDisk** |                        | **Remove-PhysicalDisk** |

  > ※***「Partition」操作には上記以外に「Resize-」もあり！***  
  > ※***フォーマット作成には「New-」の代わりに「Fortmat-Volume」がある！***

- ディスクのオンライン化と初期化

      // ディスク情報取得
      Get-Disk [-Number <ディスク番号>]

      // ディスクのオンライ化
      Set-Disk [-Number <ディスク番号>] [-IsOffline <$true / $false>]

      // ディスクの初期化
      Initialize-Disk [-Number <ディスク番号>] [-PartitionStyle <MBR / GPT>]

      // パーティションスタイルの変換
      1. diskpart
      2. list disk
      3. select disk <n>
      4. clean
      5. convert <mbr / gpt>

- パーティション操作

      // パーティション作成
      New-Partition [-DiskNumber <ディスク番号>]
                    [-NewDriveLetter <ドライブ文字>]
                    [-AssignDeiveLetter]
                    [-IsActive]
                    [-IsHidden]
                    [-Size <サイズ>]
                    [-UseMaximumSize]      

  > ※ **サイズは必ず指定する必要がある！**

      // パーティション確認
      Get-Partition [-DiskNumber <ディスク番号>]
                    [-PartitionNumber <パーティション番号>]

      // パーティション削除
      Remove-Partition [[-DiskNumber <ディスク番号>]
                       [-PartitionNumber <パーティション番号>]]
                       [-DriveLetter <ドライブ文字>]
                       [-Confirm:0]
      
      // パーティション設定変更
      Set-Partition [-DiskNumber <ディスク番号>]
                    [-PartitionNumber <パーティション番号>]
                    [-IsActive <$true / $false>]
                    [-IsHidden <$true / $false>]
                    [-NewDriveLetter <新ドライブ文字>]

      // パーティションサイズ変更
      Resize-Partition [-DiskNumber <ディスク番号>]
                       [-PartitionNumber <パーティション番号>]
                       [-Size <サイズ>]

- フォーマットとボリューム操作

      // ドライブのフォーマット
      Format-Volume [-DriveLetter <ドライブ文字>]
                    [-FileSystem <ファイルシステム>]
                    [-Full]
                    [-NewFileSystemLabel <ボリュームラベル>]
                    [-Compress]
                    [-Confirm:0]

  > ※ ***ファイルシステムは「NTFS」「ReFS」「exFAT」「FAT32」「FAT」を指定する***

      // ドライブ情報の表示
      Get-Volume [-DriveLetter <ドライブ文字>]

      // ボリュームラベルの変更
      Set-Volume [-DriveLetter <ドライブ文字>] [-NewFileSystemLabel <ボリュームラベル>]

### ■ 重複除去

- 役割と機能のインストール

      Install-WindowsFeature -ComputerName <MyNanoServer> -Name FS-Data-Deduplication

- 重複除去に関するコマンドレットの確認

      Get-Command -Module Deduplication

  > ※ ***重複除去はシステムディスクには設定できない！また、NTFSのみサポートしていて、ReFSはサポートしていないので使用付加！***

- 重複除去の有効化と無効化

      // 有効化
      Enable-DedupVolume [-Volume <ドライブ名>:]
                         [-UsageType <除去種類>]

      // 無効化
      Disable-DedupVolume [-Volume <ドライブ名>:]

      // 情報取得
      Get-DedupVolume [-Volume <ドライブ名>:]

      // 設定
      Set-DedupVolume [-Volume <ドライブ名>:]
                      [-MinimumFileAgeDays <日数>]

  > ※ ***重複除去の設定はドライブ単位で実施する！***  
  > ※ ***「-UsageType」の種類は「Default（汎用ファイルサーバ、既定値）、「Hyper-V（VDIサーバー）」、「Backup（バックアップサーバ）」の３種類！***

- 重複除去の設定

      // Set-DedupVolumeでファイルが作成されてから７日後に重複除去の対象にする設定

      Get-DedupVolume | Select-Object MinimumFileAgeDays

- 重複除去の実行

      // 開始
      Start-DedupJob [-Volume <ドライブ文字>]
                     [-Type <ジョブタイプ>]
                     [-Wait]

      // 停止
      Stop-DedupJob [-Volume <ドライブ文字>]
                    [-Type <ジョブタイプ>]

      // 取得
      Get-DedupJob [-Volume <ドライブ文字>]

  > ※ 「***ジョブタイプ」は「Optimization（重複除去）」「GarbageCollection（削除情報の清掃）」「Scrubbing（チャンクストアの検証）」「Unoptimization（重複除去解除）」の４種類***

### ■ フォルダーやファイルの管理

- Get-ChildItemとTest-Pathのパラメーター

      Get-ChildItem [[-Path] <パス>] [-Force] [-Recurse]

      Test-Path [-Path <パス>] [-PathType [<Any | Container | leaf>]

| パラメーター     | 説明                                                         |
| ---------------- | ------------------------------------------------------------ |
| **-Path <パス>** | ファイルやフォルダーを作成するパスを指定する。パイプ指定可能 |
| **-Force**       | 隠しファイルなどを含める |
| **-Recurse**     | 再起票時を指示する |
| **-PathType**    | 対象の種類を指定   |

> ※ 「***-PathType」の種類は「Any（両方、既定値）」「Container（フォルダーなど）」「Leaf（ファイルなど）」の３種類***

- フォルダーやファイルの作成

      New-Item [-Path <パス>] [-Force] [-ItemType <file | directory | SymbolicLink | Junction | HardLink>] [-Volume <オブジェクト>]

- ファイルやフォルダーのコピー

      Copy-Item [-Path <コピー元パス>] [[-Destination] <コピー先パス>] [-Force] [-Recurse]

- フォルダーやファイルの移動

      Move-Item [-Path <移動元のパス>] [-Destination <移動先のパス> [-Force]]

- フォルダーやファイルの削除

      Remove-Item [-Path <削除対処のパス>] [-Recurse] [-Force]

- フォルダーやファイルの名前の変更

      Rename-Item [-path <現在のパス>] [-NewName <新しい名前>] [-Force]

### ■ セキュリティの管理

- NTFSアクセス許可の表示

      Get-Acl [[-Path] <パス名>] [-Audit]

- フォルダーのアクセス許可の詳細表示

      (Get-Acl -Path <パス名>).Access

- アクセス許可の設定

      Set-Acl [-Path <パス名>] [-AclObject <アクセス許可>]

### ■ 共有フォルダーの管理

- 共有フォルダーに関するコマンドレットの確認

      Get-Command -Module SmbShare

- 共有フォルダの作成

      New-SmbShare [-Name <共有名>]
                   [-Path <物理フォルダー名>]
                   [-ChangeAccess <アカウント名>]
                   [-FullAccess <アカウント名>]
                   [-ReadAccedd <アカウント名>]
                   [-Description <コメント>]
                   [-FolderEnumerationMode <アクセスベース列挙>]
                   [-CachingMode <キャッシュモード>]
                   [-ConcurrentUserLimit <接続数>]

- 共有フォルダの情報確認

      Get-SmbShare [-Name <共有フォルダー名>] [-Special <$true | $false>]

- 共有設定の変更

      Set-SmbShare [-Name <共有名>]
                   [-CachingMode <キャッシングモード>]
                   [-ConcurrentUserLimit <接続数>]
                   [-Description <コメント>]
                   [-FolderEnumerationMode <アクセスベース列挙>]
                   [-Force]

- 共有フォルダーのアクセス許可の設定と確認

      // 共有フォルダーのアクセス許可エントリ
      Get-SmbShareAccess [-Name <共有フォルダー名>]

      // 共有フォルダーへのアクセスエントリの追加
      Grant-SmbShareAccess [-Name <共有フォルダー名>]
                           [-AccessRight <アクセス許可>]
                           [-AccountName <アカウント名>]
                           [-Force]

      // アクセス許可エントリの削除
      Revoke-SmbShareAccess [-Name <共有フォルダー名>]
                            [-AccountName <アカウント名>]
                            [-Force]

  > ※ ***「-AccessRight」の種類は「Full」「Modify」「Read」の３種類！***  
  > ※「***Revoke-SmbShareAccess」の削除とはつまり、アクセス拒否（＝Deny）を設定すること***

## 9. ディレクトリサービスの管理

### ■ Active Directoryドメインサービス

- オブジェクトを識別する情報

  - **Distinguished Name(DN)**  
  ・・・AD DSで各オブジェクトを一意に識別するための名前

  - **GUID(Global Unique Identifier)**  
  ・・・AD DSで各オブジェクトを一意に識別する番号（128ビット数値）

  - **SamAccountName**  
  ・・・Windows Server 2000より前のユーザーログオン名。「NetBIOSログオン名」とも呼ばれる

  - **UPN(User Principal Name)**  
  ・・・Windows Server 2000以降のユーザーログオン名。「username@domain.local」の形式

  - **SID(Security Identifier)**  
  ・・・アクセス許可の割り当てに使用する番号

- ADDSDeploymentモジュール  
  ・・・AD DSのインストール / アンインストールを行う展開用のコマンドレットを提供するモジュール。このモジュールは[Active Directoryドメインサービス]とともにインストールされる

      // ADDSDeploymentモジュールのインストール
      Install-WindowsFeature AD-Domain-Services

- ADDSDeploymentモジュールに関するコマンドレットの確認

      Get-Command -Module ADDSDeployment

- Active Directoryモジュール  
  ・・・AD DSを管理するコマンドレット。AD DSの管理ツールのインストール時に一緒にインストールされる。このモジュールがインストールされていない状態では「Import-Module Active Directory」を実行する明示的なインポートはできない。

- Active Directoryモジュールのインストール

      Install-WindowsFeature RSAT-AD-PwerShell
